/*
 * defines.h
 *
 *  Created on: 1 Aug 2018
 *      Author: matti
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define N_SAMPLES 2000
#define D_F 10 // downsampling factor
#define N_DOWNSAMPLES (N_SAMPLES/D_F)
#define N_HARMONICS 2						// Number of harmonics taken into account in reference signal
#define N_CHANNELS 3						// Number of acquisition channels
#define FS 1000								// Sample frequency
#define N_REF_SIGNALS N_HARMONICS*2
#define NTAPS 120
#define ENABLE_FILTERS 0

#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

#endif /* DEFINES_H_ */
