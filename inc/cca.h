/*
 * cca.h
 *
 *  Created on: 15 Nov 2017
 *      Author: matti
 */

#ifndef CCA_H_
#define CCA_H_



#include "defines.h"
#include "arm_math.h"



float32_t cca(int freq_index, float32_t QX[][N_DOWNSAMPLES]);
void qr_impr(float32_t X[N_CHANNELS][N_DOWNSAMPLES], float32_t Q[N_CHANNELS][N_DOWNSAMPLES]);
void qrhous(float32_t X[][N_DOWNSAMPLES], float32_t* d);
void qyhous(float32_t X[][N_DOWNSAMPLES], float32_t* i);
void transform1(float32_t X[][N_DOWNSAMPLES], int begin_index);
void transform2(float32_t X[][N_DOWNSAMPLES], float32_t* i, int begin_index);
void preprocess(float32_t* X, float32_t* pDst, arm_fir_decimate_instance_f32* FIR, arm_biquad_cascade_df2T_instance_f32* IIR);

#endif /* CCA_H_ */
