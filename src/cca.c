/*
 * cca.c
 *
 *  Created on: 15 Nov 2017
 *      Author: matti
 */

#include "dma.h"
#include "cca.h"

/*********** Includes  ****************/
#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include <math.h>

#include "svd.h"
#include "mat_ops.h"
#include "profile_stm.h"
#include "arm_math.h"


extern int cca_results[2];
extern int qr_results[2];
extern int FLAG_PROFILE_CCA;
extern int FLAG_PROFILE_QR;


//extern float32_t QYg[4][4][2000];
//extern float32_t QYg[4][4][1000];
//extern float32_t QYg[4][4][500];
//extern float32_t QYg[4][4][250];
extern float32_t QYg[4][4][200];
//extern float32_t QYg[4][4][125];
//extern float32_t QYg[4][4][100];



/**
 * n_samples: number of samples in the window
 * n_channels: number of channels processed
 * n_ref_signals: number of reference signals (sin and cos of each harmonic)
 * freq_index: the index of the frequency the input is tested for
 * X_real_ptr: pointer to the allocated input matrix
 *
 * returns: the norm of the first D cca values, where D = min(n_channels, n_ref_signals)
 *
 */


#pragma GCC push_options
#pragma GCC optimize ("O2")


float32_t cca(int freq_index, float32_t QX[][N_DOWNSAMPLES])  {

	if(FLAG_PROFILE_CCA) {
		STOPWATCH_START
	}

	int d = N_CHANNELS;	// ASSUMING WE USE AT LEAST TWO HARMONICS
	float32_t d_values[d];

	if (N_CHANNELS>=N_REF_SIGNALS) {
		float32_t QXtQY[N_CHANNELS][N_REF_SIGNALS];
		float32_t v[N_CHANNELS][N_REF_SIGNALS];
		// Multiplication: QX times QY transposed (constant defined in QYg[index]).
		// Transpose the result.
		mat_mult_t1_tres(N_SAMPLES, N_CHANNELS, QX, N_SAMPLES, N_REF_SIGNALS, QYg[freq_index], QXtQY);
		// SVD
		dsvd(N_REF_SIGNALS, N_CHANNELS, QXtQY, d_values, v);
	}
	else {
		float32_t QXtQY[N_REF_SIGNALS][N_CHANNELS];
		float32_t v[N_REF_SIGNALS][N_CHANNELS];
		// Multiplication: QX transposed times QY (constant defined in QYg[index]).
		mat_mult_t1(N_DOWNSAMPLES, N_CHANNELS, QX, N_DOWNSAMPLES, N_REF_SIGNALS, QYg[freq_index], QXtQY);
		// SVD
		dsvd(N_REF_SIGNALS, N_CHANNELS, QXtQY, d_values, v);
	}

	float32_t n = norm(d_values,0,d);
	if(FLAG_PROFILE_CCA) {
		STOPWATCH_STOP
		cca_results[0] = ((int)cca_results[0])+1;
		cca_results[1] = ((int)cca_results[1])+cyc[1];
	}
	return n;

}


void qr_impr(float32_t X[N_CHANNELS][N_DOWNSAMPLES], float32_t Q[N_CHANNELS][N_DOWNSAMPLES]) {



	if(FLAG_PROFILE_QR) {
		STOPWATCH_START
	}



	float32_t d[N_CHANNELS];
	qrhous(X, d);

	for (int col_index=0; col_index<N_CHANNELS; col_index++) {
		float32_t i[N_DOWNSAMPLES]={0}; // Column of diagonal eye matrix
		i[col_index]=1;
		qyhous(X, i);
		memcpy(&(Q[col_index]), i, N_DOWNSAMPLES*sizeof(float32_t));
		/* BEGIN DMA */
//			int n_transfers = 1;
//			DMA_Stream_TypeDef* streams[] = {DMA2_Stream0};
//			uint32_t channels[] = {DMA_Channel_0};
//			uint32_t srcAddrs[] = {i};
//			uint32_t dstAddrs[] = {&(Q[col_index])};
//			uint32_t bufferSizes[] = {N_DOWNSAMPLES};
//			uint32_t incPers[] = {1};
//			uint32_t incMems[] = {1};
//
//			DMA_Config(n_transfers, streams, channels, srcAddrs, dstAddrs, bufferSizes, incPers, incMems);
//			int TIMEOUT_MAX = 1000;
//			volatile int Timeout = TIMEOUT_MAX;
//
//			for (int transferNum=0;transferNum<n_transfers;transferNum++) {
//				while (DMA_GetCmdStatus(streams[transferNum]) != DISABLE &&(Timeout-- > 0))
//				{
//					if (Timeout == 0)
//					{
//						while (1){}
//					}
//				}
//			}
			/* END DMA */
	}

	if(FLAG_PROFILE_QR) {
		STOPWATCH_STOP
		qr_results[0] = ((int)qr_results[0])+1;
		qr_results[1] = ((int)qr_results[1])+cyc[1];
	}
}

void qrhous(float32_t X[][N_DOWNSAMPLES], float32_t* d) {

	for(int col_index=0;col_index<N_CHANNELS; col_index++) {


		float32_t s = norm(X[col_index],col_index,N_DOWNSAMPLES);
		// IF s==0 ERROR --> rank(X)<n_cols

		if(X[col_index][col_index]>=0) {
			d[col_index]=-s;
		}
		else {
			d[col_index]=s;
		}
		float32_t fak = sqrt(s*(s+fabs(X[col_index][col_index])));
		X[col_index][col_index] = X[col_index][col_index] - d[col_index];

		int blockSize = (int)(N_DOWNSAMPLES-col_index);

		arm_scale_f32(&(X[col_index][col_index]),1/fak,&(X[col_index][col_index]),blockSize);



		if(col_index<N_CHANNELS-1) {
			transform1(X, col_index);
		}
	}
}


void qyhous(float32_t X[][N_DOWNSAMPLES], float32_t* i) {

	for(int col_counter=N_CHANNELS-1; col_counter>=0; col_counter--) {
		transform2(X, i, col_counter);
	}
}


void transform1(float32_t X[][N_DOWNSAMPLES], int begin_index) {

	int new_n_rows = N_DOWNSAMPLES-begin_index;
	int new_n_cols = N_CHANNELS-(begin_index+1);

	float32_t v[N_DOWNSAMPLES] = {0};

	memcpy(&(v[begin_index]), ((float32_t*)&(X[begin_index]))+begin_index, new_n_rows*sizeof(float32_t));

	float32_t vvtM[N_CHANNELS][N_DOWNSAMPLES] = {{0}};

/* BEGIN DMA */
//	int n_transfers = 1;
//	DMA_Stream_TypeDef* streams[] = {DMA2_Stream0};
//	uint32_t channels[] = {DMA_Channel_0};
//	uint32_t srcAddrs[] = {(int)(((float32_t*)&(X[begin_index]))+begin_index)};
//	uint32_t dstAddrs[] = {(int)(&(v[begin_index]))};
//	uint32_t bufferSizes[] = {new_n_rows};
//	uint32_t incPers[] = {1};
//	uint32_t incMems[] = {1};
//
//	DMA_Config(n_transfers, streams, channels, srcAddrs, dstAddrs, bufferSizes, incPers, incMems);
//	int TIMEOUT_MAX = 1000;
//	volatile int Timeout = TIMEOUT_MAX;
//
//	for (int transferNum=0;transferNum<n_transfers;transferNum++) {
//		while (DMA_GetCmdStatus(streams[transferNum]) != DISABLE &&(Timeout-- > 0))
//		{
//			if (Timeout == 0)
//			{
//				while (1){}
//			}
//		}
//	}
	/* END DMA */

	float32_t vtM[N_CHANNELS];
	mat_mult(1, N_DOWNSAMPLES, v, N_DOWNSAMPLES, N_CHANNELS, X, vtM);

	for(int i=0; i<new_n_cols; i++) {
		uint32_t address = ((uint32_t)(&(vvtM[i+begin_index+1])))+begin_index*sizeof(float32_t);
		arm_scale_f32(&(v[begin_index]), vtM[i+begin_index+1], (float32_t*)address, new_n_rows);
	}
	mat_sub(N_DOWNSAMPLES, N_CHANNELS, X, vvtM, X);
}


void transform2(float32_t X[][N_DOWNSAMPLES], float32_t* i, int begin_index) {

	int new_n_rows = N_DOWNSAMPLES-begin_index;
	float32_t Av[new_n_rows];
	float32_t iv[new_n_rows];

	memcpy(Av, &(X[begin_index][begin_index]), new_n_rows*sizeof(float32_t));
	memcpy(iv, &(i[begin_index]), new_n_rows*sizeof(float32_t));


	/* BEGIN DMA */
//	int n_transfers = 2;
//	DMA_Stream_TypeDef* streams[] = {DMA2_Stream0,DMA2_Stream1};
//	uint32_t channels[] = {DMA_Channel_0,DMA_Channel_1};
//	uint32_t srcAddrs[] = {(uint32_t)((float32_t*)(&(X[begin_index][begin_index]))),(uint32_t)((float32_t*)(&(i[begin_index])))};
//	uint32_t dstAddrs[] = {(uint32_t)((float32_t*)Av),(uint32_t)((float32_t*)iv)};
//	uint32_t bufferSizes[] = {new_n_rows,new_n_rows};
//	uint32_t incPers[] = {1,1};
//	uint32_t incMems[] = {1,1};
//
//	DMA_Config(n_transfers, streams, channels, srcAddrs, dstAddrs, bufferSizes, incPers, incMems);
//	int TIMEOUT_MAX = 1000;
//	volatile int Timeout = TIMEOUT_MAX;
//
//	for (int transferNum=0;transferNum<n_transfers;transferNum++) {
//		while (DMA_GetCmdStatus(streams[transferNum]) != DISABLE &&(Timeout-- > 0))
//		{
//			if (Timeout == 0)
//			{
//				while (1){}
//			}
//		}
//	}
	/* END DMA */


	float32_t Avtiv;
	float32_t AvAvtiv[new_n_rows];

	arm_dot_prod_f32(Av, iv, new_n_rows, &Avtiv);
	arm_scale_f32(Av, Avtiv, AvAvtiv, new_n_rows);
	arm_sub_f32(iv, AvAvtiv, iv, new_n_rows);
	memcpy(&(i[begin_index]), iv, new_n_rows*sizeof(float32_t));

}

void preprocess(float32_t* X, float32_t* pDst, arm_fir_decimate_instance_f32* FIR, arm_biquad_cascade_df2T_instance_f32* IIR) {

	arm_fir_f32(FIR, X, pDst, (uint32_t)N_SAMPLES);
	arm_biquad_cascade_df2T_f32(IIR, pDst, pDst, (uint32_t)N_DOWNSAMPLES);
	X = pDst;
}

#pragma GCC pop_options
