/*
 * mat_ops.c
 *
 *  Created on: 23 Apr 2018
 *      Author: matti
 */

#include "mat_ops.h"
#include "arm_math.h"
#include "profile_stm.h"

#include <math.h>

extern int mat_results[6][2];
extern int FLAG_PROFILE_MAT_OPS;

#pragma GCC push_options
#pragma GCC optimize ("O2")

void mat_mult_t1_tres(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_cols_m2]) {

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};

	float mat_2t[n_rows_m2][n_cols_m2];
	arm_matrix_instance_f32 srcB_t = {n_rows_m2, n_cols_m2, (float32_t *)mat_2t};
	arm_mat_trans_f32(&srcB, &srcB_t);

	arm_matrix_instance_f32 dstC = {n_cols_m1, n_cols_m2, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcA, &srcB_t, &dstC);

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
		mat_results[0][0] = ((int)mat_results[0][0])+1;
		mat_results[0][1] = ((int)mat_results[0][1])+cyc[1];
	}

}

void mat_mult_t1(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_cols_m1]) {

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	float mat_1t[n_rows_m1][n_cols_m1];
	arm_matrix_instance_f32 srcA_t = {n_rows_m1, n_cols_m1, (float32_t *)mat_1t};
	arm_mat_trans_f32(&srcA, &srcA_t);

	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};
	arm_matrix_instance_f32 dstC = {n_cols_m2, n_cols_m1, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcB, &srcA_t, &dstC);

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_STOP
		mat_results[1][0] = ((int)mat_results[1][0])+1;
		mat_results[1][1] = ((int)mat_results[1][1])+cyc[1];
	}

}

void mat_mult(int n_rows_m1, int n_cols_m1, float mat_1[][n_rows_m1],
		int n_rows_m2, int n_cols_m2, float mat_2[][n_rows_m2],
		float mat_res[][n_rows_m1]) {

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}

	arm_matrix_instance_f32 srcA = {n_cols_m1, n_rows_m1, (float32_t *)mat_1};
	arm_matrix_instance_f32 srcB = {n_cols_m2, n_rows_m2, (float32_t *)mat_2};
	arm_matrix_instance_f32 dstC = {n_cols_m2, n_rows_m1, (float32_t *)mat_res};
	arm_mat_mult_f32(&srcB, &srcA, &dstC);

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_STOP
		mat_results[2][0] = ((int)mat_results[2][0])+1;
		mat_results[2][1] = ((int)mat_results[2][1])+cyc[1];
	}
}

void mat_sub(int n_rows, int n_cols, float a[][n_rows], float b[][n_rows], float r[][n_rows]) {


	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}

	arm_matrix_instance_f32 srcA = {n_cols, n_rows, (float32_t *)a};
	arm_matrix_instance_f32 srcB = {n_cols, n_rows, (float32_t *)b};
	arm_matrix_instance_f32 srcR = {n_cols, n_rows, (float32_t *)r};
	arm_mat_sub_f32(&srcA, &srcB, &srcR);

	if(FLAG_PROFILE_MAT_OPS) {
		 STOPWATCH_STOP
		 mat_results[3][0] = ((int)mat_results[3][0])+1;
		 mat_results[3][1] = ((int)mat_results[3][1])+cyc[1];
	}

}

float norm(float* x, int begin_index, int end_index) {

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}
	float res;
	int block_size = end_index-begin_index;
	arm_power_f32(&(x[begin_index]), block_size, &res);
	arm_sqrt_f32(res, &res);

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_STOP
		mat_results[4][0] = ((int)mat_results[4][0])+1;
		mat_results[4][1] = ((int)mat_results[4][1])+cyc[1];
	}

	return res;

}

void center_variable(float* X) {

	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_START
	}

	float vec_mean;
	arm_mean_f32(X,N_DOWNSAMPLES,&vec_mean);
	arm_offset_f32(X,-vec_mean,X,N_DOWNSAMPLES);


	if(FLAG_PROFILE_MAT_OPS) {
		STOPWATCH_STOP
		mat_results[5][0] = ((int)mat_results[5][0])+1;
		mat_results[5][1] = ((int)mat_results[5][1])+cyc[1];
	}

}

#pragma GCC pop_options
