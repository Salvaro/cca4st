/*
 * dma.c
 *
 *  Created on: 30 Jul 2018
 *      Author: matti
 */

/**
  ******************************************************************************
  * @file    main.c
  * @author  Simone Benatti & Bojan Milosevic
  * @version V1.0
  * @date    27-Apr-2017
  * @brief   DMA example
  ******************************************************************************
*/


/*********** Includes  ****************/

#include "stm32f4_discovery.h"

#include "dma.h"


/*********** Defines  *****************
#define BUFFER_SIZE              32
#define TIMEOUT_MAX              1000 /* Maximum timeout value */

//typedef enum {FAILED = 0, PASSED = !FAILED} TestStatus;


/*********** Declarations *************/
/*------ Function prototypes ---------*/
//void DMA_Config(uint32_t srcAddr, uint32_t dstAddr, uint32_t bufferSize);
//TestStatus Buffercmp(const uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t BufferLength);


/* ----- Global variables ----------- */

//volatile TestStatus  TransferStatus = FAILED;

//const uint32_t aSRC_Const_Buffer[BUFFER_SIZE]= {
//                                    0x11111111,0x22222222,0x33333333,0x44444444,
//                                    0x55555555,0x66666666,0x77777777,0x88888888,
//                                    0x99999999,0xAAAAAAAA,0xBBBBBBBB,0xCCCCCCCC,
//                                    0xDDDDDDDD,0xEEEEEEEE,0xFFFFFFFF,0x00000000,
//                                    0xCAFFE000,0xDECADECA,0x2CAFFE00,0xDECADECA,
//                                    0xBADBAD00,0xCADECADE,0x00FACCE0,0x12121212,
//                                    0x23232323,0x34343434,0x696A6B6C,0x6D6E6F70,
//                                    0x71727374,0x75767778,0x797A7B7C,0x7D7E7F80};
//
//uint32_t aDST_Buffer[BUFFER_SIZE];


//const char sorgente[14] = {"my test string"};
//char destinazione[14];

//volatile uint32_t Timeout = TIMEOUT_MAX;



/************ Main *********************/
//int main(void)
//{
//
//	/* Configure LEDs to monitor program status */
//    STM_EVAL_LEDInit(LED3);		// Orange
//    STM_EVAL_LEDInit(LED4);		// Green
//    STM_EVAL_LEDInit(LED5);		// Red
//    STM_EVAL_LEDInit(LED6);		// Blue
//
//    Timeout = TIMEOUT_MAX;
//
//    /* Configure and enable the DMA Stream for Memory to Memory transfer */
//	DMA_Config();
//
//
//	while (DMA_GetCmdStatus(DMA2_Stream0) != DISABLE &&(Timeout-- > 0))
//	{
//	  /* Check if a timeout condition occurred */
//	  if (Timeout == 0)
//	  {
//		/* Manage the error: to simplify the code enter an infinite loop */
//		while (1)
//		{
//		}
//	  }
//	/*
//	   Since this code present a simple example of how to use DMA, it is just
//	   waiting on the end of transfer.
//	   But, while DMA Stream is transferring data, the CPU is free to perform
//	   other tasks in parallel to the DMA transfer.
//	*/
//	}
//
//	/* Check if the transmitted and received data are equal */
//	TransferStatus = Buffercmp(aSRC_Const_Buffer, aDST_Buffer, BUFFER_SIZE);
//	/* TransferStatus = PASSED, if the transmitted and received data
//	 are the same */
//	/* TransferStatus = FAILED, if the transmitted and received data
//	 are different */
//
//	if (TransferStatus != FAILED)
//	{
//		/* Turn LED4 on: Transfer correct */
//		STM_EVAL_LEDOn(LED4);
//	}
//
//	while (1)
//	{
//	}
//}
//


/*********** Functions *****************/


/**
  * @brief  Configure the DMA controller according to the Stream parameters
  * @param  None
  * @retval None
  */
void DMA_Config(uint32_t nTransfers, DMA_Stream_TypeDef* stream[], uint32_t channel[], uint32_t srcAddr[], uint32_t dstAddr[], uint32_t bufferSize[], uint32_t incPer[], uint32_t incMem[])
{
	DMA_InitTypeDef  DMA_InitStructure[nTransfers];
	NVIC_InitTypeDef NVIC_InitStructure;

//	DMA_Stream_TypeDef * streamNum = DMA2_Stream0;

	/* Enable DMA clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	for(int transferNum=0;transferNum<nTransfers;transferNum++) {

	/* Reset DMA Stream registers (for debug purpose) */
	DMA_DeInit(stream[transferNum]);

	/* Check if the DMA Stream is disabled before enabling it.
	 Note that this step is useful when the same Stream is used multiple times:
	 enabled, then disabled then re-enabled... In this case, the DMA Stream disable
	 will be effective only at the end of the ongoing data transfer and it will
	 not be possible to re-configure it before making sure that the Enable bit
	 has been cleared by hardware. If the Stream is used only once, this step might
	 be bypassed. */
	while (DMA_GetCmdStatus(stream[transferNum]) != DISABLE){}

	/* Configure DMA Stream */
//	DMA_InitStructure.DMA_Channel = DMA_Channel_2;
	DMA_InitStructure[transferNum].DMA_Channel = channel[transferNum];
//	DMA_InitStructure.DMA_PeripheralBaseAddr = srcAddr;
	DMA_InitStructure[transferNum].DMA_PeripheralBaseAddr = srcAddr[transferNum];
//	DMA_InitStructure.DMA_Memory0BaseAddr = dstAddr;
	DMA_InitStructure[transferNum].DMA_Memory0BaseAddr = dstAddr[transferNum];
	DMA_InitStructure[transferNum].DMA_DIR = DMA_DIR_MemoryToMemory;
//	DMA_InitStructure.DMA_BufferSize = bufferSize;
	DMA_InitStructure[transferNum].DMA_BufferSize = bufferSize[transferNum];
	if (incPer[transferNum])
		DMA_InitStructure[transferNum].DMA_PeripheralInc = DMA_PeripheralInc_Enable;
	else
		DMA_InitStructure[transferNum].DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	if(incMem[transferNum])
		DMA_InitStructure[transferNum].DMA_MemoryInc = DMA_MemoryInc_Enable;
	else
		DMA_InitStructure[transferNum].DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA_InitStructure[transferNum].DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
	DMA_InitStructure[transferNum].DMA_MemoryDataSize = DMA_MemoryDataSize_Word;
	DMA_InitStructure[transferNum].DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure[transferNum].DMA_Priority = DMA_Priority_High;
	DMA_InitStructure[transferNum].DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure[transferNum].DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure[transferNum].DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure[transferNum].DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(stream[transferNum], &(DMA_InitStructure[transferNum]));

	}

	/* Configure DMA NVIC */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	for(int transferNum=0;transferNum<nTransfers;transferNum++) {


	/* Enable DMA Stream Transfer Complete interrupt */
	DMA_ITConfig(stream[transferNum], DMA_IT_TC, ENABLE);

	/* DMA Stream enable */
	DMA_Cmd(stream[transferNum], ENABLE);

	}

	/* Check if the DMA Stream has been effectively enabled.
	 The DMA Stream Enable bit is cleared immediately by hardware if there is an
	 error in the configuration parameters and the transfer is no started (ie. when
	 wrong FIFO threshold is configured ...) */
	for(int transferNum=0;transferNum<nTransfers;transferNum++) {
	while ((DMA_GetCmdStatus(stream[transferNum]) != ENABLE)){}
	}

}

/**
  * @brief  Compares two buffers.
  * @param  pBuffer, pBuffer1: buffers to be compared.
  * @param  BufferLength: buffer's length
  * @retval PASSED: pBuffer identical to pBuffer1
  *         FAILED: pBuffer differs from pBuffer1
  */
TestStatus Buffercmp(uint32_t* pBuffer, uint32_t* pBuffer1, uint16_t bufferLength)
{
  while(bufferLength--)
  {
    if(*pBuffer != *pBuffer1)
    {
      return FAILED;
    }

    pBuffer++;
    pBuffer1++;
  }

  return PASSED;
}


/************ Interrupt Handlers *************/

/**
  * @brief  This function handles DMA Stream interrupt request.
  * @param  None
  * @retval None
  */
void DMA2_Stream0_IRQHandler(void)
{
	/* Test on DMA Stream Transfer Complete interrupt */
	if(DMA_GetITStatus(DMA2_Stream0, DMA_IT_TCIF0))
	{
		/* Clear DMA Stream Transfer Complete interrupt pending bit */
		DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TCIF0);

		/* Turn LED3 on: End of Transfer */
		STM_EVAL_LEDOn(LED6);
	}
}
